"""
USE: python <PROGNAME> (options)
OPTIONS:
    -h : print this help message and exit
    -d FILE : use dictionary file FILE
    -i FILE : process text from input file FILE
    -o FILE : write results to output file FILE
"""
################################################################

import sys, getopt
import binascii
import io

################################################################

MAXWORDLEN = 5

################################################################
# Command line options handling, and help

opts, args = getopt.getopt(sys.argv[1:], 'hd:i:o:')
opts = dict(opts)

def printHelp():
    progname = sys.argv[0]
    progname = progname.split('/')[-1] # strip out extended path
    help = __doc__.replace('<PROGNAME>', progname, 1)
    print('-' * 60, help, '-' * 60, file=sys.stderr)
    sys.exit()
"""    
if '-h' in opts:
    printHelp()

if len(args) > 0:
    print("\n** ERROR: no arg files - only options! **", file=sys.stderr)
    printHelp()

if '-d' not in opts:
    print("\n** ERROR: must specify dictionary (opt: -d)! **", file=sys.stderr)
    printHelp()

if '-i' not in opts:
    print("\n** ERROR: must specify input text file (opt: -i)! **", file=sys.stderr)
    printHelp()

if '-o' not in opts:
    print("\n** ERROR: must specify output text file (opt: -o)! **", file=sys.stderr)
    printHelp()
"""
################################################################


def main():
    with open('chinesetrad_wordlist.utf8', 'rb') as f:
        word_list = []
        for line in f:
            word_list.append(line.decode('utf8')[:-1])

    with open('chinesetext.utf8', 'rb') as f:
        test_data = []
        for line in f:
            test_data.append(line.decode('utf8')[:-1])
    
    with io.open('results.utf8', 'w', encoding='utf8') as output:
        for line in test_data:
            # line = test_data[9]
            # 我 到 她 家 等候
            prospective_word = ''
            i = 0
            # print(len(line))
            largest_word = ''
            lw_start = 0
            while i < len(line):
                # print(line[i])
                prospective_word += line[i]
                # print(f"{line[i]} {prospective_word} {len([ word for word in word_list if word.startswith(prospective_word) ]) < 2}")
                if prospective_word in word_list:
                    largest_word = prospective_word

                if len(prospective_word) == MAXWORDLEN or len([ word for word in word_list if word.startswith(prospective_word) ]) < 2:
                    # print(largest_word)
                    if largest_word == '':
                        largest_word = prospective_word
                    output.write(largest_word + ' ')
                    lw_start += len(largest_word)
                    largest_word = ''
                    prospective_word = ''
                    i = lw_start
                    # print(i)
                else:
                    i += 1
            if len(prospective_word) != 0:
                output.write(prospective_word)
            output.write('\n')

    # with io.open('results.utf8', 'w', encoding='utf8') as output:
    #     for line in test_data[:5]:
    #         print(line)
    #         i_word_start = 0
    #         prospective_word = ''
    #         largest_word = ''
    #         for i in range(len(line)):
    #             print(i)
    #             if prospective_word == '':
    #                 i_word_start = i
                
    #             prospective_word += line[i]
    #             for word in word_list:
    #                 if prospective_word == word and len(word) > len(largest_word):
    #                     largest_word = word
            
    #         output.write(largest_word)
    #         output.write(' ')
    #         prospective_word = ''
    #         largest_word = ''
    #         i = i_word_start + len(largest_word)
                    
    #         output.write('\n')
    #         print()
    #         print()
        
            
            
            # """
            # prospective_word = ''
            # for c in line:
            #     prospective_word += c
            #     largest_word = ''
                
                
                
            #     for word in word_list:
            #         if len(word) > MAXWORDLEN
                    
            #         if prospective_word == word and len(word) > len(largest_word):
            #             largest_word = word
            #             output.write(prospective_word)
            #             output.write(' ')
            #             prospective_word = ''
            #             break
            #     else:
            #         if len(prospective_word) == MAXWORDLEN:
            #             output.write(prospective_word)
            #             output.write(' ')
            #             prospective_word = ''
            # """

if __name__ == '__main__':
    main()
